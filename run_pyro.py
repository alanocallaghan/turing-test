import pyreadr
import json
import pandas
import matplotlib.pyplot as plt
import seaborn
from numpyro.infer import MCMC, NUTS, HMC

from statistics import mean, median
import numpy
import numpyro
import jax
import scipy
import math
import gt_nb_pyro

with open('../baseqtl-simulation/bjs.json') as f:
    bjs = json.load(f)
with open('../baseqtl-simulation/g.json') as f:
    g = json.load(f)
Y_mat = pandas.read_feather('../baseqtl-simulation/Y_mat.feather').to_numpy()
# Y_mat = numpy.array(Y_mat)
cov = pyreadr.read_r('../baseqtl-simulation/cov.rds')[None]
cov = numpy.array(cov)
covsub = numpy.array([x[2] for x in cov])
g = numpy.array(g)
bjs = numpy.array(bjs)
Y = Y_mat[0]

# x = gt_nb_pyro.run_gt_nb(Y_mat[0], g, cov, 2)
# plt.plot(range(len(x.losses)), x.losses)
# plt.show()

estmcmc = []


# kernel = numpyro.infer.SA(gt_nb_pyro.gt_nb)
# kernel = numpyro.infer.BarkerMH(gt_nb_pyro.gt_nb)
# kernel = HMC(gt_nb_pyro.gt_nb)
kernel = NUTS(gt_nb_pyro.gt_nb)


mcmc=MCMC(
    kernel,
    num_samples = 5000,
    num_warmup = 5000
    # ,
    # progress_bar = False
)
Yrand = scipy.stats.nbinom(math.exp(20), 0.5).rvs(200)
import rpy2.robjects as robjects
r = robjects.r
mu = r["rlnorm"](200, meanlog = 10)
p = r["rgamma"](1, shape=1, rate=1)
Y_rand = r["rnbinom"](200, mu = mu, size = p)
Y_rand = numpy.array(Y_rand)
mcmc.run(
    Y = Yrand,
    # Y = Y_mat[625],
    cov = covsub,
    g = g,
    rng_key = jax.random.PRNGKey(42)
)

# estmcmc.append(mcmc.get_samples()["bj"])


sam = mcmc.get_samples()

seaborn.pairplot(
    pandas.DataFrame(sam),
    kind = "kde"
)
plt.show()


plt.plot(range(5000), mcmc.get_samples()["beta"])
plt.plot(range(5000), mcmc.get_samples()["phi"])
plt.show()



ests = [gt_nb_pyro.run_gt_nb(Y_mat[i], g, cov, 5)[0] for i in range(100)]
estss = [x["bj_auto_loc"] for x in ests]
p = plt.scatter(range(0, 100), ests)
p = plt.scatter(bjs[0:100], ests)
plt.show()





# with open('../wallace/baseqtl-simulation/out.json') as f:
#     x = json.load(f)

# with open('tmp.json') as f:
#     x = json.load(f)

# with open('posterior.json') as f:
#     posterior = json.load(f)



# out = []
# for y in x:
#     out.append(run_gt_nb(y))

# run_gt_nb(x[0])
# run_gt_nb(x[272])

# with open("../wallace/baseqtl-simulation/res.json", "w") as f:
#     json.dump(out, f)
