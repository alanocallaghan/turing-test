using Random
using Turing
using Turing: Variational

Random.seed!(42);

# generate data
x = randn(2000);


@model function model(x)
    s ~ InverseGamma(2, 3)
    m ~ Normal(0.0, sqrt(s))
    for i = 1:length(x)
        x[i] ~ Normal(m, sqrt(s))
    end
end;

m = model(x);

samples_nuts = sample(m, NUTS(200, 0.65), 10_000);

advi = ADVI(10, 1000);
q = vi(m, advi);

