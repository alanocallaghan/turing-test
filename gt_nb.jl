using Random
using Turing
using RData
using Turing: Variational

Random.seed!(42);

rng = MersenneTwister(1234);

function NegativeBinomial2(μ, ϕ)
    p = 1 / (1 + μ / ϕ)
    r = ϕ

    return NegativeBinomial(r, p)
end

nb = rand(rng, NegativeBinomial(), 10000);

@model function nb1(x)
    r ~ Gamma()
    p ~ Uniform()
    x ~ NegativeBinomial(
        r,
        p
    )
end
@model function nb2(x)
    mu ~ Exponential()
    phi ~ Exponential()
    x ~ NegativeBinomial2(
        mu,
        phi
    )
end

m1 = nb1(nb);
s1 = sample(m1, NUTS(200, 0.65), 10_000);
m2 = nb2(nb);
s2 = sample(m2, NUTS(200, 0.65), 10_000);


x = load("tmp.rds");

cov = x["cov"][:, 2:3];
Y = x["Y"];
g = x["g"];

@model function gt_nb(cov, Y, g)
    
    N = size(Y, 1)
    K = size(cov, 2)
    k = 2
    aveP = [0, 0]
    sdP = [0.0309, 0.3479]
    mixP = [0.9736, 0.0264]

    # bj ~ Normal()
    bj ~ MixtureModel(
        Normal,
        [(aveP[j], sdP[j]) for j = 1:k],
        mixP
    )

    l1pebj = log1p(exp(bj)) - log(2)
    phi ~ Gamma(1, 0.01)
    # betas = Array{Float64}(undef, K)
    alpha ~ Normal(6, 4)
    beta ~ Cauchy(0, 2.5)
    lmu = alpha .+ cov[:, 2] * beta
    for i = 1:N
        lmu[i] = abs(g[i])==1 ? lmu[i] + l1pebj : lmu[i];
        lmu[i] = g[i]==2 ? lmu[i] + bj : lmu[i];
        Y[i] ~ NegativeBinomial2(exp(lmu[i]), phi);
    end
end

m = gt_nb(cov, Y, g);
samples_nuts = sample(m, NUTS(200, 0.65), 10_000);

advi = ADVI(10, 1000);
q = vi(m, advi);

_, sym2range = bijector(m, Val(true));
sym2range

samples = rand(q, 10000);

avg_vi = vec(mean(samples; dims = 2))
avg_hmc = vec(mean(samples_nuts; dims = 2))

