import numpyro
import numpy
import jax
from numpyro.distributions import Gamma, Normal
from numpyro.infer import MCMC, NUTS, HMC
from numpyro.infer import SVI, Trace_ELBO, RenyiELBO
from statistics import median
import matplotlib.pyplot as plt

def simple(x):
    mu = numpyro.sample("mu", Normal(0, 1))
    sigma = numpyro.sample("sigma", Gamma(1, 1))
    numpyro.sample("obs", Normal(mu, sigma), obs = x)
    # for i in numpyro.plate("x", len(x)):

def run_simple(x):
    # optim = pyro.optim.ClippedAdam({'lr': initial_lr, 'lrd': lrd})
    optim = numpyro.optim.Adam(step_size=0.001)
    
    guide = numpyro.infer.autoguide.AutoDiagonalNormal(simple)
    svi = SVI(
        simple, 
        guide,
        optim = optim,
        # loss = Trace_ELBO()
        loss = RenyiELBO(num_particles=10)
    )
    # # guide_gt_nb.median(Y, g, cov)
    svi_result = svi.run(jax.random.PRNGKey(0), 10000, x=x)
    return svi_result


x1 = numpy.random.normal(size=100)
x2 = (numpy.random.normal(size=100)*2) + 5

res1 = run_simple(x1)
res2 = run_simple(x2)


plt.plot(range(len(res1.losses)), res1.losses)
plt.show()

plt.plot(range(len(res2.losses)), res2.losses)
plt.show()


kernel = NUTS(simple, adapt_step_size = True)

mcmc=MCMC(
    kernel,
    num_samples = 1000,
    num_warmup = 1000
)
mcmc.run(x = x1, rng_key = jax.random.PRNGKey(42))
s1 = mcmc.get_samples()
mcmc.run(x = x2, rng_key = jax.random.PRNGKey(42))
s2 = mcmc.get_samples()




plt.plot(range(1000), s1["mu"])
plt.show()
plt.plot(range(1000), s2["mu"])
plt.show()
