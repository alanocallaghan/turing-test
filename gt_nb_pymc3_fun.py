#!/usr/bin/env python3

import theano.tensor as tt
import pymc3 as pm
import pymc3.math as pmath
import numpy

def gt_nb(x):
    Y = x["Y"]
    g = x["g"]
    cov = x["cov"]
    cov = numpy.array(cov)
    l = len(cov) / 3
    cov = cov[int(l*2):len(cov)]

    with pm.Model() as model:
        N = len(Y)
        mu = numpy.array([0, 0])
        sigma = numpy.array([0.0309, 0.3479])
        weights = numpy.array([0.9736, 0.0264])
        bj = pm.NormalMixture("bj", w = weights, mu = mu, sigma = sigma)
        l1pebj = pmath.log(pmath.exp(bj) + 1) - pmath.log(2)
        phi = pm.Gamma("phi", 1, 0.01)
        alpha = pm.Normal("alpha", 6, 4)
        beta = pm.Normal("beta", 0, 2.5)
        lmu = alpha + (cov * beta)
        for i in range(1, N):
            if abs(g[i] == 1):
                tt.set_subtensor(lmu[i], lmu[i] + l1pebj)
            if g[i] == 2:
                tt.set_subtensor(lmu[i], lmu[i] + bj)
        obs = pm.NegativeBinomial(
            "obs",
            mu = pmath.exp(lmu),
            alpha = phi,
            observed = Y,
            shape = N
        )
        fit = pm.fit(method = "advi")
        samples = fit.sample(1000)
    
    return(
        [samples.get_values(x) for x in ('alpha', 'beta', 'bj', 'phi')]
    )
