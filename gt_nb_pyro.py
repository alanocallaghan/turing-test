import typing
import jax
import math
import numpy

import numpyro
from numpyro.distributions import (
    Gamma,
    NegativeBinomial2,
    Normal,
    Cauchy,
    TruncatedNormal,
    LogNormal,
    Exponential,
    InverseGamma
)
# from numpyro import plate, sample
from numpyro import distributions as dist
from numpyro.infer import (
    SVI,
    Trace_ELBO,
    TraceGraph_ELBO,
    TraceMeanField_ELBO,
    RenyiELBO,
    autoguide
)
from numpyro.optim import ClippedAdam

from statistics import median

from numpy import array


# from pyro.distributions import TorchDistribution
# from torch.distributions import constraints
# from torch import tensor, Size
    
# https://forum.pyro.ai/t/mixture-of-poissons-with-large-sample-size/1901
# class TruncatedMixtureNormal(TorchDistribution):
#     support = dist.Normal.support
#     has_rsample = False

#     def __init__(self, mu, sigma, theta, low, high):
#         self._normal = TruncatedNormal(mu, sigma, low = low, high = high)
#         self.theta = theta
#         self._num_component = theta.shape[-1]

#         cdbs = self._normal.batch_shape[:-1]
#         event_shape = self._normal.event_shape
#         self._event_ndims = len(event_shape)

#         super(MixtureNormal, self).__init__(batch_shape=cdbs,
#                                             event_shape=event_shape,
#                                             validate_args=False)

#     def sample(self, sample_shape=Size()):
#         return ones(sample_shape + self.shape())

#     def log_prob(self, value):
#         contributions = self._normal.log_prob(value) + self.theta.log()
#         return contributions.logsumexp(-1)


def gt_nb(Y, cov, g):
    # bj = sample("bj", TruncatedMixtureNormal(
    #         mu = tensor([0, 0]),
    #         sigma = tensor([0.0309, 0.3479]),
    #         theta = tensor([0.9736, 0.0264]),
    #         low = -10,
    #         high = 10
    #     )
    # )
    # bj = numpyro.sample("bj", TruncatedNormal(loc = 0, scale = 1, low = -10, high = 10))
    bj = numpyro.sample("bj", Normal(0, 1))
    # phi = numpyro.sample("phi", LogNormal(0, 1))
    phi = numpyro.sample("phi", Gamma(1, 0.01))
    # phi = numpyro.sample("phi", Exponential(1))
    # phi = numpyro.sample("phi", InverseGamma(1, 0.01))
    # phi = 5
    alpha = numpyro.sample("alpha", Normal(6, 4))
    beta = numpyro.sample("beta", Cauchy(0, 2.5))
    lmu = alpha + (cov * beta)
    l1pebj = jax.numpy.log1p(jax.numpy.exp(bj)) - jax.numpy.log(2)

    for i in range(len(Y)):
        if abs(g[i]) == 1:
            lmu.at[i].set(lmu[i] + l1pebj)
        if g[i] == 2:
            lmu.at[i].set(lmu[i] + bj)

    with numpyro.plate("y", len(Y)):
        numpyro.sample(
            "obs",
            # Normal(lmu, phi),
            NegativeBinomial2(mean = jax.numpy.exp(lmu), concentration = phi),
            obs = Y
        )

## Y is a N numpy vec
## g is a N numpy vec
## cov is a N x 3 numpy array
def run_gt_nb(Y: numpy.array, g: numpy.array, cov: numpy.array, num_particles: int):

    # Y = x["Y"]
    # g = x["g"]
    # cov = x["cov"]
    # cov = numpy.array(cov)
    # g = numpy.array(g)
    # Y = numpy.array(Y)
    # l = len(cov) / 3
    # cov = cov[int(l*2):len(cov)]
    # cov = torch.from_numpy(cov)
    # cov = torch.tensor([x[2] for x in cov])
    cov = numpy.array([x[2] for x in cov])
    # g = torch.from_numpy(g)
    # Y = torch.from_numpy(Y)


    # http://pyro.ai/examples/svi_part_iv.html
    initial_lr = 0.001
    gamma = 0.1  # final learning rate will be gamma * initial_lr
    lrd = gamma ** (1 / 10000)
    # optim = ClippedAdam({'lr': initial_lr, 'lrd': lrd})
    optim = numpyro.optim.Adam(step_size=0.0005)
    
    # guide_gt_nb = autoguide.AutoDiagonalNormal(gt_nb_pyro.gt_nb)
    # AutoBNAFNormal
    # AutoIAFNormal
    # AutoDelta
    guide_gt_nb = autoguide.AutoDelta(gt_nb)
    svi = SVI(
        model = gt_nb, 
        # model = gt_nb_pyro.gt_nb, 
        guide = guide_gt_nb,
        optim = optim,
        loss = RenyiELBO(num_particles=num_particles)
        # loss = Trace_ELBO(num_particles=2)
    )
    
    # Trace_ELBO, TraceGraph_ELBO, TraceMeanField_ELBO, RenyiELBO
    svi_result = svi.run(jax.random.PRNGKey(0), 10000, Y=Y, cov=cov, g=g)
    return svi_result
