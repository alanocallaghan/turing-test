import jax
import numpy

import numpyro
from numpyro.distributions import (
    Gamma,
    NegativeBinomial2,
    Normal
)
from numpyro.infer import (
    SVI,
    Trace_ELBO,
    autoguide
)
from numpyro.infer import MCMC, NUTS

def nb(Y):
    mu = numpyro.sample("mu", Normal(0, 1))
    phi = numpyro.sample("phi", Gamma(1, 0.01))

    with numpyro.plate("y", len(Y)):
        numpyro.sample(
            "obs",
            NegativeBinomial2(mean = jax.numpy.exp(mu), concentration = phi),
            obs = Y
        )

def run_nb(Y):
    optim = numpyro.optim.Adam(step_size=0.0005)

    guide_gt_nb = autoguide.AutoDelta(nb)
    svi = SVI(
        model = nb, 
        guide = guide_gt_nb,
        optim = optim,
        loss = Trace_ELBO()
    )
    
    svi_result = svi.run(jax.random.PRNGKey(0), 10000, Y=Y)
    return svi_result


import scipy
import math
import seaborn
import pandas
import matplotlib.pyplot as plt


phi = scipy.stats.gamma(1, 0.01).rvs()
mu = 5
p = phi
n = mu / (sigma - pow(mu, 2))
sigma = mu / p
Y = scipy.stats.nbinom(n, p).rvs(200)
Y = [scipy.stats.nbinom(math.exp(scipy.stats.norm(5, 1).rvs()), phi).rvs() for i in range(200)]
Y = numpy.array(Y)

app = run_nb(Y)

kernel = NUTS(nb)

mcmc=MCMC(
    kernel,
    num_samples=1000,
    num_warmup=1000
)
mcmc.run(
    Y = Y,
    rng_key = jax.random.PRNGKey(42)
)
# estmcmc.append(mcmc.get_samples()["bj"])


sam = mcmc.get_samples()

seaborn.pairplot(
    pandas.DataFrame(sam),
    kind = "kde"
)
plt.show()

