#!/usr/bin/env python3

import theano.tensor as tt
import pymc3 as pm
import pymc3.math as pmath
import numpy
import seaborn
import json
import matplotlib.pyplot as plt
import rpy2.robjects as robjects
from rpy2.robjects import r
from rpy2.robjects.packages import importr


with open('tmp.json') as f:
    x = json.load(f)



Y = x["Y"]
g = x["g"]
cov = x["cov"]
cov = numpy.array(cov)
l = len(cov) / 3
cov = cov[int(l*2):len(cov)]



with pm.Model() as model:
    N = len(Y)
    mu = numpy.array([0, 0])
    sigma = numpy.array([0.0309, 0.3479])
    weights = numpy.array([0.9736, 0.0264])
    bj = pm.NormalMixture("bj", w = weights, mu = mu, sigma = sigma)
    l1pebj = pmath.log(pmath.exp(bj) + 1) - pmath.log(2)
    phi = pm.Gamma("phi", 1, 0.01)
    alpha = pm.Normal("alpha", 6, 4)
    beta = pm.Normal("beta", 0, 2.5)
    lmu = alpha + (cov * beta)
    for i in range(1, N):
        if abs(g[i] == 1):
            tt.set_subtensor(lmu[i], lmu[i] + l1pebj)
        if g[i] == 2:
            tt.set_subtensor(lmu[i], lmu[i] + bj)
    obs = pm.NegativeBinomial(
        "obs",
        mu = pmath.exp(lmu),
        alpha = phi,
        observed = Y,
        shape = N
    )
    # svgd = pm.fit(method = "svgd")
    # asvgd = pm.fit(method = "asvgd")
    # nfvi = pm.fit(method = "nfvi")
    advi = pm.fit(method = "advi")
    # advifr = pm.fit(method = "fullrank_advi")
    # -   'advi'  for ADVI
    # -   'fullrank_advi'  for FullRankADVI
    # -   'svgd'  for Stein Variational Gradient Descent
    # -   'asvgd'  for Amortized Stein Variational Gradient Descent
    # -   'nfvi'  for Normalizing Flow with default `scale-loc` flow
    # -   'nfvi=<formula>'  for Normalizing Flow using formula
    hmc = pm.sample(5000, tune = 5000, target_accept = 0.9, cores = 4)



def extract(fit):
    samples = fit.sample(1000)
    return(
        [samples.get_values(x) for x in ('alpha', 'beta', 'bj', 'phi')]
    )


hmc_samples = [hmc.get_values(x) for x in ('alpha', 'beta', 'bj', 'phi')]

# svgd_samples = extract(svgd)
# asvgd_samples = extract(asvgd)
# nfvi_samples = extract(nfvi)
advi_samples = extract(advi)
# advifr_samples = extract(advifr)


# numpy.mean(hmc_samples, 1)
# numpy.mean(svgd_samples, 1)
# numpy.mean(asvgd_samples, 1)
# numpy.mean(nfvi_samples, 1) ## v bad
numpy.mean(advi_samples, 1)
# numpy.mean(advifr_samples, 1)
